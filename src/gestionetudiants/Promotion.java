/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionetudiants;

import java.util.ArrayList;

public class Promotion {
    private ArrayList<Etudiant> listeEtud = new ArrayList<Etudiant>();
    private String nom;
    private int annee;
    
    public Promotion(String _nom, int _annee){
        nom = _nom;
        annee = _annee;
    }
    public String getNom() {
	return nom;
    }
    public ArrayList<Etudiant> getListeEtud() {
	return listeEtud;
    }
    public Etudiant getEtud(int _i) { 
        return listeEtud.get(_i); 
    } 
    public void ajouterEtud(Etudiant _e){
        listeEtud.add(_e);
    }
    private double moyenne() {
        double somme = 0;
        if(listeEtud.size() == 0) return 0.0;
        for (Etudiant e:listeEtud){
            somme += e.moyenne();
        }
        return somme/listeEtud.size();
    }
    public String toString(){
        String s = new String();
        s += "Promotion " + this.annee + " : \n";
        for (Etudiant e:listeEtud)
            s += e + "\n";
        s += "Moyenne générale: " + this.moyenne() + "\n"; 
        return s;
    }    
    
//    public static void main(String[] args) {
//       Promotion p2016 = new Promotion(2016);
//       Etudiant et1 = new Etudiant("Dupont","Pierre", 20);
//       Etudiant et2 = new Etudiant("Blanc","Simon", 19);
//       Etudiant et3 = new Etudiant("Renaud","Megane",22);            
//       p2016.ajouterEtud(et1);
//       p2016.ajouterEtud(et2);
//       p2016.ajouterEtud(et3);
//       et1.ajouterNote(14);
//       et1.ajouterNote(11);
//       et2.ajouterNote(13);
//       et3.ajouterNote(16);
//       et3.ajouterNote(13);
//       System.out.println(p2016);
//    }    
}
