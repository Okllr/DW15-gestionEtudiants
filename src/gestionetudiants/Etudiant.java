/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionetudiants;

import java.util.ArrayList;

public class Etudiant {
    private String nom, prenom;
    private int age;
    public double moyenne;
    private ArrayList<Integer> notes = new ArrayList<Integer>();
    
    public Etudiant(String _nom, String _prenom, int _age) {
	nom = _nom;
	prenom = _prenom;
	age = _age;
    }
    public String getNom() {
	return nom;
    }
    void setAge(int _age) {
	age = _age;
    }
    public String toString() {
        return nom + ", " + prenom + " (" +  age + " ans) - Notes : " + notes 
                + " Moyenne : " + this.moyenne();
    }
    public void ajouterNote(int _n){
        double somme = 0;
        notes.add(_n);
        if(notes.size() == 1){ 
            moyenne = _n;
        } else {
            for (int n:notes){
                somme += n;
            }
            moyenne = somme/notes.size(); 
        }
    }  
//    public void ajouterNotes(ArrayList<Integer> _notes){
//        notes.addAll(_notes);
//    } 
    public double moyenne() {
        return moyenne;
    }
    
//    public static void main(String[] args) {
//        ArrayList<Etudiant> listeEtud = new ArrayList<Etudiant>();      
//        listeEtud.add(new Etudiant("Popovsky","Boris",18));
//        listeEtud.add(new Etudiant("Lantier","Etienne",19));
//        listeEtud.add(new Etudiant("Bouley","Stephane",20));
//        listeEtud.get(0).ajouterNote(14);
//        listeEtud.get(0).ajouterNote(11);
//        listeEtud.get(1).ajouterNote(15);
//        listeEtud.get(2).ajouterNote(15);
//        listeEtud.get(2).ajouterNote(12);
//        System.out.println("Etudiants: ");
//        System.out.println(listeEtud);
//    }
}
