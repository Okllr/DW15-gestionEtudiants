/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestionetudiants;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Menu {
    private ArrayList<Promotion> promos = new ArrayList<Promotion>();
    
    private ArrayList<Etudiant> etuds = new ArrayList<Etudiant>();
    
    public void ajouterPromo(Promotion _promo){
        promos.add(_promo);
    }
    public Promotion getPromo(int _i) { 
        return promos.get(_i); 
    } 
    public ArrayList<Promotion> getAllPromos() { 
        return promos; 
    } 
    public ArrayList<Etudiant> getAllEtuds() { 
        ArrayList<Etudiant> allEtuds = new ArrayList<Etudiant>();
        for(Promotion promo : promos){
            for(Etudiant etud : promo.getListeEtud()){
                allEtuds.add(etud);
            }
        }
        return allEtuds;
    }
    public void ajoutEtud(Etudiant _e){
        etuds.add(_e);
    }
    public void fusionPromotion (Promotion p1, Promotion p2, Promotion nomPromotionDeFusion){
        for (Etudiant e : p1.getListeEtud()){
            nomPromotionDeFusion.ajouterEtud(e);
	}
	for (Etudiant e : p2.getListeEtud()){
            nomPromotionDeFusion.ajouterEtud(e);
	}
    }
    public String getDefault() {
        return "Commande inconnue, veuillez recommencer";
    }
    public String getIndispo() {
        return "Choix non disponible \n";
    }
    public String getStripe() {
        return "\n\n-----------------------------\n\n";
    }
    public String afficher() {
        return "Bienvenue dans mon programme: \n\n"
                + "1. Créer un nouvel étudiant \n"
                + "2. Créer une nouvelle promotion \n"
                + "3. Afficher les étudiants d'une promotion \n"
                + "4. Afficher le meilleur étudiant par promotion \n"
                + "5. Ajouter des notes à un étudiant \n"
                + "6. Fusionner deux promotions \n"
                + "7. Trier les étudiants d'une promotion \n"
                + "8. Fonctionnalité surprise ! \n\n"
                + "Votre choix:";
    }
    public void phraseChoix(int _i) {
        String opt;
        switch (_i) {
            case 1:
                opt = "de créer un nouvel étudiant.";
                break;
            case 2:
                opt = "de créer une nouvelle promotion.";
                break;
            case 3:
                opt = "les étudiants d'une promotion.";
                break;
            case 4:
                opt = "le meilleur étudiant par promotion.\n"
                        + "Sélectionnez parmi les promotions dispnibles:\n";
                break;
            case 5:
                opt = "d'ajouter des notes à un étudiant.\n"
                        + "Entrez le nom de l'étudiant:";
                break;
            case 6:
                opt = "de fusionner deux promotions.";
                break;
            case 7:
                opt = "de trier les étudiants d'une promotion.\n"
                        + "Sélectionnez parmi les promotions disponibles:`\n";
                break;
            case 8:
                opt = "de supprimer un étudiant.";
                break;
            default:
                opt = "une option inconnue ...";
        }
        System.out.println("\nVous avez choisi " + opt);
    }

    public static void main(String[] args) {
        // init App
        Menu myMenu = new Menu();
        Scanner scan = new Scanner(System.in);
        Boolean femerApp = false;
       
        // init Content 
        // promo 2014
        Promotion p2014 = new Promotion("LProSTic2014",2014);
        myMenu.ajouterPromo(p2014);
        p2014.ajouterEtud(new Etudiant("Dufour","André", 54));
        p2014.getEtud(0).ajouterNote(9);
        p2014.getEtud(0).ajouterNote(8);
        p2014.ajouterEtud(new Etudiant("Rimini","Dominique", 38));
        p2014.getEtud(1).ajouterNote(15);
        p2014.getEtud(1).ajouterNote(12);   
        p2014.ajouterEtud(new Etudiant("Mounier","Théo", 25));
        p2014.getEtud(2).ajouterNote(17);
        p2014.getEtud(2).ajouterNote(13);
        p2014.getEtud(2).ajouterNote(6);    
        // promo 2015
        Promotion p2015 = new Promotion("LProSTic2015",2015);
        myMenu.ajouterPromo(p2015);
        p2015.ajouterEtud(new Etudiant("Ravier","Sandrine", 34));
        p2015.getEtud(0).ajouterNote(13);
        p2015.getEtud(0).ajouterNote(15);
        p2015.ajouterEtud(new Etudiant("Thomson","Steve", 26));
        p2015.getEtud(1).ajouterNote(11);
        p2015.getEtud(1).ajouterNote(16);   
        p2015.ajouterEtud(new Etudiant("Apfel","Loïc", 39));
        p2015.getEtud(2).ajouterNote(8);
        p2015.getEtud(2).ajouterNote(16); 
        p2015.getEtud(2).ajouterNote(12); 
        p2015.ajouterEtud(new Etudiant("Pemerier","Nathalie", 19));
        p2015.getEtud(3).ajouterNote(17);
        p2015.getEtud(3).ajouterNote(10);  
        
        // Application
        System.out.println(myMenu.afficher());
        while (femerApp == false) {
            int i = scan.nextInt();
            if (i >= 1 && i <= 8) {
                switch (i) {
                    case 1:
                        myMenu.phraseChoix(i);
                        System.out.println("Nom:");
                        String s1Opt1 = scan.next();
                        System.out.println("Prénom:");
                        String s2Opt1 = scan.next();
                        System.out.println("Âge:");
                        int yOpt1 = scan.nextInt();
                        Etudiant newEtud = new Etudiant(s1Opt1,s2Opt1,yOpt1);
                        myMenu.ajoutEtud(newEtud);
                        int etudIdxOpt1 = myMenu.getAllEtuds().size();
                        System.out.println("Entrez les notes (-1 pour terminer)");
                        boolean fermerOpt1 = false;
                        while(fermerOpt1 == false){                            
                            int y2Opt1 = scan.nextInt();
                            if(y2Opt1 >=1 && y2Opt1 <= 20){
                                newEtud.ajouterNote(etudIdxOpt1);
                                System.out.println("Note ajoutée");
                            }else if(y2Opt1 == -1) {
                                fermerOpt1 = true;
                            }else {
                                System.out.println(myMenu.getIndispo());
                            }
                        }
                        System.out.println(myMenu.getStripe() + myMenu.afficher());
                        break;
                    case 2:
                        myMenu.phraseChoix(i);    
                        System.out.println("Nom:");
                        String sOpt2 = scan.next();
                        System.out.println("Année:");
                        int yOpt2 = scan.nextInt();
                        Promotion newPromo = new Promotion(sOpt2,yOpt2);
                        myMenu.ajouterPromo(newPromo);
                        System.out.println("\n"+"Sélectionnez parmi les étudiants disponibles:");
                        for(int idxOpt2=0; idxOpt2 < myMenu.getAllEtuds().size(); idxOpt2++){
                            System.out.println((idxOpt2 + 1) + " - " + myMenu.getAllEtuds().get(idxOpt2));
                        } 
                        System.out.println("\nEntrez un numéro (-1 pour terminer)");
                        boolean fermerOpt2 = false;
                        while(fermerOpt2 == false){                            
                            int y2Opt2 = scan.nextInt();                                 
                            if(y2Opt2 >=1 && y2Opt2 < myMenu.getAllEtuds().size()){
                                newPromo.ajouterEtud(myMenu.getAllEtuds().get((y2Opt2 - 1)));
                                System.out.println("Etudiant ajouté");
                            }else if(y2Opt2 == -1) {
                                fermerOpt2 = true;
                            }else {
                                System.out.println(myMenu.getIndispo());
                            }
                        }
                        System.out.println(myMenu.getStripe() + myMenu.afficher());                        
                        break;
                    case 3:
                        myMenu.phraseChoix(i);
                        for (int idxOpt3=0; idxOpt3 < myMenu.getAllPromos().size(); idxOpt3++) {
                            System.out.println((idxOpt3 + 1) + " - " + myMenu.getPromo(idxOpt3).getNom());
                        } 
                        System.out.println("\nVotre choix:");
                        int yOpt3 = scan.nextInt();
                        if(yOpt3 >= 1 && yOpt3 <= myMenu.getAllPromos().size()){
                            System.out.println(myMenu.getPromo(yOpt3 - 1).getListeEtud());
                        }
                        break;
                    case 4:
                        myMenu.phraseChoix(i);
                        for (int idxOpt4=0; idxOpt4 < myMenu.getAllPromos().size(); idxOpt4++) {
                            System.out.println((idxOpt4 + 1) + " - " + myMenu.getPromo(idxOpt4).getNom());
                        } 
                        System.out.println("\nVotre choix:");
                        int yOpt4 = scan.nextInt();
                        if(yOpt4 >= 1 && yOpt4 <= myMenu.getAllPromos().size()){
                            ArrayList<Etudiant> listeEtud = myMenu.getPromo(yOpt4 - 1).getListeEtud();
                            int bestEtudIdx = 0;
                            double bestMoyenne = 0;
                            for (int idx2Opt4=0; idx2Opt4 < listeEtud.size(); idx2Opt4++) {
                                if(idx2Opt4 == 0){
                                    bestMoyenne = listeEtud.get(idx2Opt4).moyenne();
                                } else {
                                    if(bestMoyenne < listeEtud.get(idx2Opt4).moyenne()){
                                       bestEtudIdx = idx2Opt4;
                                       bestMoyenne = listeEtud.get(idx2Opt4).moyenne();
                                    }
                                } 
                            }
                            System.out.println("\n"+"Le meilleur étudiant de cette promotion est:\n"
                                + listeEtud.get(bestEtudIdx) 
                                + myMenu.getStripe() + myMenu.afficher()
                            );                            
                        } else {
                            System.out.println(myMenu.getIndispo() + myMenu.getStripe() + myMenu.afficher());
                        }                        
                        break;
                    case 5:
                        myMenu.phraseChoix(i);
                        int etudIdx = -1;
                        String sOpt5  = scan.next();                        
                        for(int idxOpt5=0; idxOpt5 < myMenu.getAllEtuds().size(); idxOpt5++){
                           if(sOpt5.toUpperCase().equals(myMenu.getAllEtuds().get(idxOpt5).getNom().toUpperCase())){
                                etudIdx = idxOpt5;
                                break;
                           }
                        } 
                        if(etudIdx == -1){
                            System.out.println("\nAucun étudiant portant ce nom n'a été trouvé.");
                        } else {
                            System.out.println("\nEntrez les notes à ajouter (-1 pour terminer)");
                            boolean fermerOpt5 = false;
                            while(fermerOpt5 == false){                            
                                int yOpt5 = scan.nextInt();                                 
                                if(yOpt5 >=0 && yOpt5 <= 20){
                                    myMenu.getAllEtuds().get(etudIdx).ajouterNote(yOpt5);  
                                    System.out.println("Note ajoutée");
                                }else if(yOpt5 == -1) {
                                    fermerOpt5 = true;
                                }else {
                                    System.out.println(myMenu.getIndispo());
                                }
                            } 
                        }
                        System.out.println(myMenu.getStripe() + myMenu.afficher());
                        break;
                    case 6:
                        myMenu.phraseChoix(i);
                        int promoFusion1 = -1;
                        int promoFusion2 = -1;
                        for (int idxOpt6=0; idxOpt6 < myMenu.getAllPromos().size(); idxOpt6++) {
                            System.out.println((idxOpt6 + 1) + " - " + myMenu.getPromo(idxOpt6).getNom());
                        } 
                        System.out.println("\nPremier choix:");
                        int sOpt6 = scan.nextInt();
                        if(sOpt6 >= 1 && sOpt6 <= myMenu.getAllPromos().size()){
                            promoFusion1 = sOpt6 - 1;    
                            System.out.println("\nSecond choix:");
                            int s2Opt6 = scan.nextInt();
                            if(s2Opt6 >= 1 && s2Opt6 <= myMenu.getAllPromos().size()){
                                promoFusion2 = s2Opt6 - 1;      
                                System.out.println("\nNom de la nouvelle Promotion:");
                                String s3Opt6 = scan.next();
                                Promotion newPromoFusion = new Promotion(s3Opt6,2016); 
                                myMenu.ajouterPromo(newPromoFusion);
                                myMenu.fusionPromotion(myMenu.getPromo(promoFusion1),myMenu.getPromo(promoFusion2),newPromoFusion);                            
                                System.out.println("\nFusion des promotions effectuée" + myMenu.getStripe() + myMenu.afficher());
                            } else {
                                System.out.println(myMenu.getIndispo() + myMenu.getStripe() + myMenu.afficher());
                            }
                        } else {
                            System.out.println(myMenu.getIndispo() + myMenu.getStripe() + myMenu.afficher());
                        }                        
                        break;
                    case 7:
                        myMenu.phraseChoix(i);
                        for (int idxOpt7=0; idxOpt7 < myMenu.getAllPromos().size(); idxOpt7++) {
                            System.out.println((idxOpt7 + 1) + " - " + myMenu.getPromo(idxOpt7).getNom());
                        } 
                        System.out.println("\nVotre choix:");
                        int yOpt7 = scan.nextInt();
                        if(yOpt7 >= 1 && yOpt7 <= myMenu.getAllPromos().size()){
                            List<Etudiant> listePromoEtud = myMenu.getPromo(yOpt7 - 1).getListeEtud();                            
                            Comparator<Etudiant> ascEtudMoyenne = (Etudiant e1, Etudiant e2) -> (int) (double) (e1.moyenne() - e2.moyenne()); // Ascending
                            Collections.sort( listePromoEtud, ascEtudMoyenne);                           
                            System.out.println("\n"+"Liste des étudiants triée par moyennes croissantes:");
                            for(Etudiant etud : listePromoEtud){
                                System.out.println(etud);
                            };
                            System.out.println(myMenu.getStripe() + myMenu.afficher());                          
                        } else {
                            System.out.println(myMenu.getIndispo() + myMenu.getStripe() + myMenu.afficher());
                        }                        
                        break;
                    case 8:
                        myMenu.phraseChoix(i);
                        System.out.println("Nom:");
                        String sOpt8 = scan.next();
                        int etudIdxOpt8 = -1;
                        for(int idxOpt8=0; idxOpt8 < myMenu.getAllEtuds().size(); idxOpt8++){
                           if(sOpt8.toUpperCase().equals(myMenu.getAllEtuds().get(idxOpt8).getNom().toUpperCase())){
                                etudIdxOpt8 = idxOpt8;
                                break;
                           }
                        } 
                        if(etudIdxOpt8 == -1){
                            System.out.println("\nAucun étudiant portant ce nom n'a été trouvé.");
                        } else {
                            myMenu.getAllEtuds().remove(etudIdxOpt8);
                            System.out.println("\nÉtudiant supprimé.");
                        }
                        System.out.println(myMenu.getStripe() + myMenu.afficher());
                        break;
                    default:
                        System.out.println("Commande en cours de création ...");
                }
            } else if (i == 999) {
                femerApp = true;
            } else {
                System.out.println(myMenu.getDefault());
            }
        }
    }
}
